﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Globalization;

using GCI.TextImport;

namespace XrfWindowsService
{
    public partial class XrfWindowsService : ServiceBase
    {
        PollingWorker pr;
        private static int numLogOutputLines = 0;
        private static List<RockWiseProfileData> listProfile = new List<RockWiseProfileData>();
        private static int? lessThanValueFactor = 2;
        private static bool showLessThanSym = false;
        private static int runningProc = 0;
        private static int endedProc = 0;
        private static CultureInfo currentCi = CultureInfo.GetCultureInfo(1033);
        private static RockWiseCorrectionMask corrMask = new RockWiseCorrectionMask();

        public XrfWindowsService()
        {
            InitializeComponent();

            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory); 
            pr = new PollingWorker(5000, XrfWindowsService.CheckAndProcess, null);
        }

        static XrfWindowsService()
        {
            try
            {
                if (Properties.Settings.Default.lessValueOption > 1)
                {
                    lessThanValueFactor = Properties.Settings.Default.lessValueOption;
                    showLessThanSym = false;
                }
                else if (Properties.Settings.Default.lessValueOption == 0)
                {
                    lessThanValueFactor = (int?)null;
                    showLessThanSym = false;
                }
                else if (Properties.Settings.Default.lessValueOption == 1)
                {
                    lessThanValueFactor = 1;
                    showLessThanSym = true;
                }

                CSVParseSettings csvSettingsUsed = new CSVParseSettings('\t', '\0', (char)34, (char)34, '.', true, 0);
                listProfile = ImportRawData.ParseCsvTextFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XrfPreprocessorProfiles.txt"), csvSettingsUsed, true).ToRockWiseDataList<RockWiseProfileData>(lessThanValueFactor, showLessThanSym, currentCi);
                //DataView dataView = new DataView(listProfile.ToElemDataTable(false, "", "", false));
                //showElementsDataInGrid(dgvProfile, dataView, false, false);

                //cmbProfile.SelectedIndexChanged -= new EventHandler(cmbProfile_SelectedIndexChanged);
                //cmbProfile.DataSource = listProfile;
                //cmbProfile.DisplayMember = "ProfileName";
                //cmbProfile.SelectedIndexChanged += new EventHandler(cmbProfile_SelectedIndexChanged);

                //string delimStr = " ,_";
                //pickProfile(listStd.First().ProfileNameFromOrigFile.ToString().Split(delimStr.ToCharArray()).FirstOrDefault());
                //pickProfile();
                //generateCaliSets();

                //if (projXrf != null)
                //{
                //    if (listStd.Count > 0 && listProfile.Count > 0) reloadProjectStdProfiles();
                //    if (listRaw.Count > 0 && listProfile.Count > 0) reloadProjectRawProfiles();
                //}
                //dataView = new DataView(listStd.ToElemDataTable(false, "", "", true));
                //dataView.Sort = " EvalTime";
                //showAssignedStandardsInGroups(gridControl2, dataView, true, "ProfileAssigned");
            }
            catch
            {
            }
        }

        protected override void OnStart(string[] args)
        {
            pr.StartWork(true);
        }

        protected override void OnStop()
        {
            pr.PauseWork();
        }

        protected override void OnPause()
        {
            base.OnPause();
            pr.PauseWork();
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            pr.StartWork();
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
            pr.Quit();
        }

        private static void LogToFile(string eventText)
        {
            string logFileNameBase = Properties.Settings.Default.logFileName;
            // Collect the sort command output. 
            if (!String.IsNullOrEmpty(eventText))
            {
                numLogOutputLines++;

                string logfile = logFileNameBase + DateTime.Now.ToString("_MMddyyyy") + ".log";
                if (!File.Exists(logfile))
                {
                    File.Create(logfile).Close();
                    numLogOutputLines = 0;
                }

                File.AppendAllText(logfile, Environment.NewLine +
                    "[" + numLogOutputLines.ToString() + "] - " + DateTime.Now.ToString() + " - " + eventText);
            }
        }

        private static void CheckAndProcess(object state)
        {
            List<RockWiseMeasuredData> listRaw = new List<RockWiseMeasuredData>();
            List<RockWiseCalibrationData> listStd = new List<RockWiseCalibrationData>();
            //List<CalibrationSet> cals = new List<CalibrationSet>();
            List<RockWiseMeasuredData> listCorrectedRaw = new List<RockWiseMeasuredData>();
            AlgorithmEnum alg = (AlgorithmEnum)Properties.Settings.Default.correctionMethod;
            int algRange = Properties.Settings.Default.correctionSamples;
            //Dictionary<string, double> csvOut = new Dictionary<string, double>();
            //double dnumber;

            try
            {
                string rawLoc = Properties.Settings.Default.rawFilesDir;
                //string topasLoc = Properties.Settings.Default.lessValueOption;
                ////string fileINP = Properties.Settings.Default.fileINP;
                //string fileTopasOutput = Properties.Settings.Default.fileTopasOutput;
                //string fileTopasOutputRWP = Properties.Settings.Default.fileTopasOutputRWP;
                //string fileCSV = Properties.Settings.Default.fileCSV;

                if (Directory.Exists(rawLoc))
                {
                    List<string> listOfFiles = new List<string>();
                    //List<string> processedNames = new List<string>();

                    listOfFiles = Directory.GetFiles(rawLoc).ToList();

                    foreach (string filen in listOfFiles.Where(f => !Path.GetFileNameWithoutExtension(f).ToLower().EndsWith("std")))
                    {
                        string fileName = Path.GetFileNameWithoutExtension(filen);
                        //if (!processedNames.Contains(fileName))
                        //{
                        if (listOfFiles.Any((s) => Path.GetFileName(s).ToLower().StartsWith(fileName.ToLower() + " std")))
                        {

                            string stdFile = listOfFiles.Find((s) => Path.GetFileName(s).ToLower().StartsWith(fileName.ToLower() + " std"));
                            LogToFile("Processing files - " + filen + " and " + stdFile);

                            try
                            {
                                CSVParseSettings csvSettingsUsed = new CSVParseSettings();
                                listRaw = ImportRawData.ParseCsvTextFile(filen, new CSVParseSettings('\t', '\0', '"', '"', '.', true, 3), false, out csvSettingsUsed, false).ToRockWiseDataList<RockWiseMeasuredData>(lessThanValueFactor, showLessThanSym, currentCi);
                                listRaw.ForEach(s => s.ProfileAssigned = listProfile.Find(f => f.ProfileName.ToLower() == "sdc-1"));

                                csvSettingsUsed = new CSVParseSettings();
                                listStd = ImportRawData.ParseCsvTextFile(stdFile, new CSVParseSettings('\t', '\0', '"', '"', '.', true, 3), false, out csvSettingsUsed, false).ToRockWiseDataList<RockWiseCalibrationData>(lessThanValueFactor, showLessThanSym, currentCi);
                                //lblStdCount.Text = listStd.Count + " records loaded";

                                //lblStdRecords.Text = listStd.Where(o => o.Select).Count() + "/" + listStd.Count;

                                //txtRawDataFile.Enabled = true;
                                //btnRawDataFile.Enabled = true;

                                //reloadProjectStdExclusions();

                                //string delimStr = " ,_";
                                //listStd.ForEach(s => s.ProfileAssigned = listProfile.Where(f => f.ProfileName.ToLower() == s.ProfileNameFromOrigFile.ToString().Split(delimStr.ToCharArray()).FirstOrDefault().ToLower()).Count() > 0 ? listProfile.Where(f => f.ProfileName.ToLower() == s.ProfileNameFromOrigFile.ToString().Split(delimStr.ToCharArray()).FirstOrDefault().ToLower()).First() : null);
                                listStd.ForEach(s => s.ProfileAssigned = listProfile.Find(f => f.ProfileName.ToLower()=="sdc-1"));
                                //listStd.Where(s=>s.Elements[3].NormValue.HasValue).Where(s=>s.Elements[3].NormValue.Value>80).ToList().ForEach(s => s.ProfileAssigned.. = listProfile.Find(f => f.ProfileName.ToLower() == "sdc-1"));

                                //if (listStd.Any(s=>s.Elements[3].NormValue>80))
                                //{
                                //    CalibrationSet ca = new CalibrationSet();
                                //    ca = new CalibrationSet(listStd.Where(s=>s.Elements[3].NormValue>80).ToList(), listProfile.Find(f => f.ProfileName.ToLower()=="gbw 07106"));
                                //    //cals.Add(ca);
                                //    listStd.Where(s=>s.Elements[3].NormValue>80).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
                                //}

                                //if ((listStd.Any(s => s.Elements[8].NormValue > 5 && s.Elements[8].NormValue<=40)) || listStd.Any(s => s.Elements[1].NormValue > 3))
                                //{
                                //    CalibrationSet ca = new CalibrationSet();
                                //    ca = new CalibrationSet(listStd.Where(s => (s.Elements[8].NormValue > 5 && s.Elements[8].NormValue <= 40) || s.Elements[1].NormValue > 3).ToList(), listProfile.Find(f => f.ProfileName.ToLower() == "nist-88b"));
                                //    //cals.Add(ca);
                                //    listStd.Where(s => (s.Elements[8].NormValue > 5 && s.Elements[8].NormValue <= 40) || s.Elements[1].NormValue > 3).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
                                //}

                                //if (listStd.Any(s => s.Elements[8].NormValue > 40))
                                //{
                                //    CalibrationSet ca = new CalibrationSet();
                                //    ca = new CalibrationSet(listStd.Where(s => s.Elements[8].NormValue > 40).ToList(), listProfile.Find(f => f.ProfileName.ToLower() == "nist-1d"));
                                //    //cals.Add(ca);
                                //    listStd.Where(s => s.Elements[8].NormValue > 40).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
                                //}

                                //if (listStd.Any(s => s.Elements[1].NormValue > 3))
                                //{
                                //    CalibrationSet ca = new CalibrationSet();
                                //    ca = new CalibrationSet(listStd.Where(s => s.Elements[8].NormValue > 3).ToList(), listProfile.Find(f => f.ProfileName.ToLower() == "nist-88b"));
                                //    cals.Add(ca);
                                //    listStd.Where(s => s.Elements[8].NormValue > 40).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
                                //}

                                //List<RockWiseCalibrationData> listStdFiltered = listStd.Where(c => c.Select).ToList();
                                //List<RockWiseProfileData> profilesRedefined = listStd.Where(o => o.ProfileAssigned != null).Select(s => s.ProfileAssigned).Distinct().ToList();
                                //cals.Clear();
                                //List<CalibrationSet> calsNotExistAnymore = cals.Except(cals.Where(c => profilesRedefined.Select(p => p.ProfileName).Contains(c.ProfileName))).ToList();
                                //cals.RemoveAll(c => !profilesRedefined.Select(p => p.ProfileName).Contains(c.ProfileName));



                                //foreach (RockWiseProfileData f in profilesRedefined)
                                //{
                                //    CalibrationSet ca = new CalibrationSet();
                                //    if (cals.Select(c => c.ProfileName).Contains(f.ProfileName))
                                //    {
                                //        ca = cals.Where(c => c.ProfileName == f.ProfileName).First();
                                //        ca.ChangeCalibrationSetPoints(listStd.Where(s => s.ProfileAssigned == f).ToList());
                                //        listStd.Where(s => s.ProfileAssigned == f).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
                                //    }
                                //    else
                                //    {
                                //        ca = new CalibrationSet(listStd.Where(s => s.ProfileAssigned == f).ToList(), f);
                                //        cals.Add(ca);
                                //        listStd.Where(s => s.ProfileAssigned == f).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
                                //    }
                                //}

                                //List<RockWiseCalibrationData> listStdFiltered = listStd.Where(c => c.Select).Where(s => s.ProfileAssigned != null).ToList();
                                //List<RockWiseMeasuredData> listRawFiltered = listRaw.Where(c => c.Select).Where(s => s.ProfileAssigned != null).ToList();
                                listCorrectedRaw = listRaw.Select(c => c.Clone()).ToList();
                                //listCorrectedRaw.ForEach(c => c.ProfileAssigned = listRawFiltered.Where(r => r.EvalTime == c.EvalTime).First().ProfileAssigned);
                                //listCorrectedRaw.ForEach(s => s.ProfileAssigned = listProfile.Find(f => f.ProfileName.ToLower() == "sdc-1"));

                                //int num;
                                if (alg == AlgorithmEnum.SampleNumber)
                                {
                                    foreach (RockWiseMeasuredData rawd in listCorrectedRaw)
                                    {
                                        createnNormCalisetCalueMod(listStd.OrderBy(c => (c.EvalTime - rawd.EvalTime).Duration()).Take(algRange).ToList());
                                        CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStd.OrderBy(c => (c.EvalTime - rawd.EvalTime).Duration()).Take(algRange).ToList());
                                        //CalibrationSet ncals = CalibrationSet.ChangeCalibrationSetPointsAndAverage(listStdFiltered.Where(s => s.ProfileAssigned == rawd.ProfileAssigned).OrderBy(c => (c.EvalTime - rawd.EvalTime).Duration()).Take(algRange).ToList());
                                        rawd.PerformCorrection(corrMask, ncals);
                                    }
                                }
                                else if (alg == AlgorithmEnum.TimeBlock)
                                {
                                    DateTime start1 = listRaw.Min(c => c.EvalTime);
                                    DateTime start2 = listStd.Min(c => c.EvalTime);
                                    DateTime start = DateTime.Compare(start1, start2) < 0 ? start1 : start2;

                                    start1 = listRaw.Max(c => c.EvalTime);
                                    start2 = listStd.Max(c => c.EvalTime);
                                    DateTime end = DateTime.Compare(start1, start2) < 0 ? start2 : start1;

                                    TimeSpan interval = TimeSpan.FromHours(algRange);
                                    for (DateTime current = start; current <= end; current += interval)
                                    {
                                        foreach (RockWiseMeasuredData rawd in listCorrectedRaw.Where(c => c.EvalTime >= current && c.EvalTime < current + interval))
                                        {
                                            createnNormCalisetCalueMod(listStd.Where(c => c.EvalTime >= current && c.EvalTime < current + interval).ToList());
                                            CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStd.Where(c => c.EvalTime >= current && c.EvalTime < current + interval).ToList());
                                            //CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStdFiltered.Where(c => c.EvalTime >= current && c.EvalTime < current + interval && c.ProfileAssigned == rawd.ProfileAssigned).ToList());
                                            rawd.PerformCorrection(corrMask, ncals);
                                        }
                                    }
                                }
                                else if (alg == AlgorithmEnum.ManualTimeBlock)
                                {
                                    listStd = listStd.Where(c => c.Select).ToList();
                                    DateTime start = listStd.Min(c => c.EvalTime);
                                    DateTime end = listStd.Max(c => c.EvalTime);
                                    TimeSpan tp = end - start;
                                    
                                    //DataTable table = new DataTable();
                                    //table.Columns.Add("Start", typeof(DateTime));
                                    //table.Columns.Add("End", typeof(DateTime));
                                    TimeSpan result = TimeSpan.FromTicks(tp.Ticks / algRange);

                                    DateTime start1 = listRaw.Min(c => c.EvalTime);
                                    DateTime start2 = listStd.Min(c => c.EvalTime);
                                    DateTime rstart = DateTime.Compare(start1, start2) < 0 ? start1 : start2;

                                    start1 = listRaw.Max(c => c.EvalTime);
                                    start2 = listStd.Max(c => c.EvalTime);
                                    DateTime rend = DateTime.Compare(start1, start2) < 0 ? start2 : start1;

                                    //DateTime rcurrent = rstart;
                                    for (DateTime current = start; (end - current).TotalSeconds >= 1; current += result)
                                    {
                                        //DataRow row = table.NewRow();
                                        //row["Start"] = current;
                                        //row["End"] = current + result;
                                        //table.Rows.Add(row);

                                        DateTime blockEnd = current + result;
                                        if ((end - blockEnd).TotalSeconds < 1)
                                        {
                                            blockEnd = rend;
                                            foreach (RockWiseMeasuredData rawd in listCorrectedRaw.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd))
                                            {
                                                createnNormCalisetCalueMod(listStd.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd).ToList());
                                                CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStd.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd).ToList());
                                                //CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStdFiltered.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd && c.ProfileAssigned == rawd.ProfileAssigned).ToList());
                                                rawd.PerformCorrection(corrMask, ncals);
                                            }
                                        }
                                        else if (current==start)
                                        {
                                            current = rstart;
                                            foreach (RockWiseMeasuredData rawd in listCorrectedRaw.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd))
                                            {
                                                createnNormCalisetCalueMod(listStd.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd).ToList());
                                                CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStd.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd).ToList());
                                                //CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStdFiltered.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd && c.ProfileAssigned == rawd.ProfileAssigned).ToList());
                                                rawd.PerformCorrection(corrMask, ncals);
                                            }
                                            current = start;
                                        }
                                        else
                                        {
                                            foreach (RockWiseMeasuredData rawd in listCorrectedRaw.Where(c => c.EvalTime >= current && c.EvalTime < blockEnd))
                                            {
                                                createnNormCalisetCalueMod(listStd.Where(c => c.EvalTime >= current && c.EvalTime < blockEnd).ToList());
                                                CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStd.Where(c => c.EvalTime >= current && c.EvalTime < blockEnd).ToList());
                                                //CalibrationSet ncals = CalibrationSet.AverageCalibrationSetPoints(listStdFiltered.Where(c => c.EvalTime >= current && c.EvalTime < blockEnd && c.ProfileAssigned == rawd.ProfileAssigned).ToList());
                                                rawd.PerformCorrection(corrMask, ncals);
                                            }
                                        }
                                        //current = blockEnd;
                                    }
                                    //splitTimeBlocks();




                                    //for (int i = 0; i < dgvTimeZones.Rows.Count; i++)
                                    //{
                                    //    DateTime blockEnd = (DateTime)dgvTimeZones.Rows[i].Cells["End"].Value;
                                    //    if (i == dgvTimeZones.Rows.Count - 1)
                                    //    {
                                    //        blockEnd = end;
                                    //        foreach (RockWiseMeasuredData rawd in listCorrectedRaw.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd))
                                    //        {
                                    //            CalibrationSet cals = CalibrationSet.ChangeCalibrationSetPointsAndAverage(listStdFiltered.Where(c => c.EvalTime >= current && c.EvalTime <= blockEnd && c.ProfileAssigned == rawd.ProfileAssigned).ToList());
                                    //            rawd.PerformCorrection(corrMask, cals);
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        foreach (RockWiseMeasuredData rawd in listCorrectedRaw.Where(c => c.EvalTime >= current && c.EvalTime < blockEnd))
                                    //        {
                                    //            CalibrationSet cals = CalibrationSet.ChangeCalibrationSetPointsAndAverage(listStdFiltered.Where(c => c.EvalTime >= current && c.EvalTime < blockEnd && c.ProfileAssigned == rawd.ProfileAssigned).ToList());
                                    //            rawd.PerformCorrection(corrMask, cals);
                                    //        }
                                    //    }
                                    //    current = blockEnd;
                                    //}
                                }

                                System.Windows.Forms.Form sWin = new System.Windows.Forms.Form();
                                System.Windows.Forms.DataGridView dgvCorrected=new System.Windows.Forms.DataGridView();
                                dgvCorrected.AllowUserToAddRows = false;
                                sWin.Controls.Add(dgvCorrected);
                                DataView dataView = new DataView(listCorrectedRaw.ToElemDataTable(false, "", "", false));
                                dataView.Sort = " Depth";
                                showElementsDataInGrid(dgvCorrected, dataView, false, false);
                                //btnExportCorrected.Enabled = true;
                                //btnSaveCorrected2Csv.Enabled = true;
                                
                                string csvStr=ExtensionHelper.generateCsvString(dgvCorrected, true, "Select");

                                string csvFileDir = Properties.Settings.Default.fileCSVDir;
                                string logfile = Path.Combine(csvFileDir, fileName+"_corrected" + DateTime.Now.ToString("_MMddyyyy") + ".csv");
                                if (!File.Exists(logfile))
                                {
                                    File.Create(logfile).Close();
                                }
                                else
                                {
                                    csvStr = csvStr.Substring(csvStr.IndexOf(Environment.NewLine) + 1);
                                }
                                if (!String.IsNullOrEmpty(csvStr))
                                {
                                    File.AppendAllText(logfile, csvStr);
                                }
                            }
                            catch
                            {
                            }
                            DirectoryInfo di = new DirectoryInfo(rawLoc);
                            DirectoryInfo dis = di.CreateSubdirectory("ProcessedFilesPair");

                            moveFile(filen, di.GetDirectories("ProcessedFilesPair").First().FullName);
                            moveFile(stdFile, di.GetDirectories("ProcessedFilesPair").First().FullName);
                        }
                    }
                }
            }
            catch
            {
            }
        }

        private static void moveFile(string f, string path)
        {
            if (File.Exists(Path.Combine(path,Path.GetFileName(f)))) 
            {
                File.Delete(Path.Combine(path, Path.GetFileName(f)));
            }
            System.IO.File.Move(f, Path.Combine(path,Path.GetFileName(f)));
        }

        private static void createnNormCalisetCalueMod(List<RockWiseCalibrationData> listStd)
        {
            CalibrationSet ca = new CalibrationSet();
            ca = new CalibrationSet(listStd, listProfile.Find(f => f.ProfileName.ToLower() == "sdc-1"));
            listStd.ForEach(a => a.ElementsCalibrationSet = ca);

            if (listStd.Any(s => s.Elements[3].NormValue > 80))
            {
                CalibrationSet ca1 = listStd.Where(s => s.Elements[3].NormValue > 80).First().ElementsCalibrationSet.Clone();
                ca1[3].AcceptedValueMod = listProfile.Find(f => f.ProfileName.ToLower() == "gbw 07106").Elements[3].NormValue.Value;
                ca1.ChangeCalibrationSetPoints(listStd.Where(s => s.Elements[3].NormValue > 80).ToList());
                listStd.Where(s => s.Elements[3].NormValue > 80).ToList().ForEach(a => a.ElementsCalibrationSet = ca1);
            }

            if (listStd.Any(s => s.Elements[8].NormValue > 5 && s.Elements[8].NormValue <= 40))
            {
                CalibrationSet ca1 = listStd.Where(s => s.Elements[8].NormValue > 5 && s.Elements[8].NormValue <= 40).First().ElementsCalibrationSet.Clone();
                ca1[8].AcceptedValueMod = listProfile.Find(f => f.ProfileName.ToLower() == "nist-88b").Elements[8].NormValue.Value;
                ca1.ChangeCalibrationSetPoints(listStd.Where(s => s.Elements[8].NormValue > 5 && s.Elements[8].NormValue <= 40).ToList());
                listStd.Where(s => s.Elements[8].NormValue > 5 && s.Elements[8].NormValue <= 40).ToList().ForEach(a => a.ElementsCalibrationSet = ca1);
            }

            if (listStd.Any(s => s.Elements[1].NormValue > 3))
            {
                CalibrationSet ca1 = listStd.Where(s => s.Elements[1].NormValue > 3).First().ElementsCalibrationSet.Clone();
                ca1[1].AcceptedValueMod = listProfile.Find(f => f.ProfileName.ToLower() == "nist-88b").Elements[1].NormValue.Value;
                ca1.ChangeCalibrationSetPoints(listStd.Where(s => s.Elements[1].NormValue > 3).ToList());
                listStd.Where(s => s.Elements[1].NormValue > 3).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
            }

            if (listStd.Any(s => s.Elements[8].NormValue > 40))
            {
                CalibrationSet ca1 = listStd.Where(s => s.Elements[8].NormValue > 40).First().ElementsCalibrationSet.Clone();
                ca1[8].AcceptedValueMod = listProfile.Find(f => f.ProfileName.ToLower() == "nist-1d").Elements[8].NormValue.Value;
                ca1.ChangeCalibrationSetPoints(listStd.Where(s => s.Elements[8].NormValue > 40).ToList());
                listStd.Where(s => s.Elements[8].NormValue > 40).ToList().ForEach(a => a.ElementsCalibrationSet = ca1);
            }
            ca.ChangeCalibrationSetPoints(listStd.Where(s => s.ElementsCalibrationSet == ca).ToList());
        }

        private static void showElementsDataInGrid(System.Windows.Forms.DataGridView dgv, DataView data, bool showSelectColumn, bool allowToggleColumns)
        {
            dgv.AutoGenerateColumns = true;
            dgv.ColumnHeadersVisible = true;
            dgv.DataSource = null;
            dgv.DataSource = data;
            foreach (System.Windows.Forms.DataGridViewColumn dc in dgv.Columns)
            {
                if (allowToggleColumns)
                {
                    DatagridViewCheckBoxHeaderCell chkHeader = new DatagridViewCheckBoxHeaderCell();
                    dc.HeaderCell = chkHeader;
                    chkHeader.Checked = true;
                }
                if (dc.Name == "Select")
                {
                    DatagridViewCheckBoxHeaderCell chkHeader = new DatagridViewCheckBoxHeaderCell();
                    dc.HeaderCell = chkHeader;
                    chkHeader.Checked = true;
                    //chkHeader.OnCheckBoxClicked += new CheckBoxClickedHandler(chkHeader_OnCheckBoxClicked);
                    dc.Visible = showSelectColumn;
                    dc.ReadOnly = false;
                }
                else
                {
                    dc.ReadOnly = true;
                }
            }

            //if (dgv == dgvCorrected)
            //    foreach (int i in defaultCorrColumnsUnselected)
            //        ((DatagridViewCheckBoxHeaderCell)dgv.Columns[i].HeaderCell).Checked = false;

        }
    }
}
