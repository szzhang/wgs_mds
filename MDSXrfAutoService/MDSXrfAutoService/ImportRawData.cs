﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.IO;
using System.Globalization;
using System.Windows.Forms;

using GCI.TextImport;
using LumenWorks.Framework.IO.Csv;
using IsoCorelib;

namespace XrfWindowsService
{
    public class ImportRawData
    {
        /* parse CSV silently */
        public static DataTable ParseCsvTextFile(string filePath, CSVParseSettings csvSettings,bool hasHeaderActual)
        {
            try
            {
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //using (CsvReader csv = new CsvReader(new StreamReader(fs), false,
                //    csvSettings.Delimiter, csvSettings.Quote, csvSettings.Escape, csvSettings.Comment, false))
                StreamReader streamReader = new StreamReader(fs);
                string allText = streamReader.ReadToEnd();
                string strUsed = Strings.RemoveLinesFromBeginning(allText, csvSettings.NumberOfLinesIgnored);
                using (CsvReader csv = new CsvReader(new StringReader(strUsed), hasHeaderActual,
                    csvSettings.Delimiter, csvSettings.Quote, csvSettings.Escape, csvSettings.Comment, false))
                {
                    csv.MissingFieldAction = MissingFieldAction.ReplaceByEmpty;
                    csv.SkipEmptyLines = true;
                    csv.DefaultParseErrorAction = ParseErrorAction.AdvanceToNextLine;
                    csv.SupportsMultiline = csvSettings.IfSupportMultiline;
                    DataTable dt = new DataTable(Path.GetFileName(filePath));
                    dt.Locale = (csvSettings.DecimalMark == ';') ? new CultureInfo("de-DE") : new CultureInfo("en-US");
                    int i;
                    int fieldCount = csv.FieldCount;
                    for (i = 0; i < fieldCount; i++)
                    {
                        DataColumn dc = new DataColumn();
                        string headerName = string.Format("Col[{0}]", i);
                        dc.ColumnName = hasHeaderActual ? csv.GetFieldHeaders()[i] : headerName;
                        dt.Columns.Add(dc);
                    }
                    while (csv.ReadNextRecord())
                    {
                        DataRow dr = dt.NewRow();
                        for (i = 0; i < fieldCount; i++)
                        {
                            dr[i] = csv[i];
                        }
                        dt.Rows.Add(dr);
                    }
                    streamReader.Close();
                    fs.Close();
                    return TrimDataTable(dt);
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /* parse CSV with dialog */
        public static DataTable ParseCsvTextFile(string filePath, CSVParseSettings csvSettings,
            bool ifSilent, out CSVParseSettings csvSettingsUsed, bool hasHeaderActual)
        {
            //out variables
            csvSettingsUsed = new CSVParseSettings();

            //parse csv
            try
            {
                if (!ifSilent && csvSettings.IsEmpty)
                {
                    //get parameters
                    string sampleData = CSVFileGetParametersDialog.GetFileSampleData(filePath);
                    CSVFileGetParametersDialog d = new CSVFileGetParametersDialog(sampleData, filePath);
                    if (d.ShowDialog() == DialogResult.OK)
                    {
                        //load
                        csvSettingsUsed = new CSVParseSettings(d.Delimiter, d.Comment, d.Escape,
                            d.Quote, d.DecimalMark[0], d.IfSupportMultiline, d.NLinesIgnored);
                        return ParseCsvTextFile(filePath, csvSettingsUsed,hasHeaderActual);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    if (csvSettings.IsEmpty)
                    {
                        //throw new Exception("Invalid CSV settings");
                        string sampleData = CSVFileGetParametersDialog.GetFileSampleData(filePath);
                        csvSettings = CSVFileGetParametersDialog.GuessCSVParseSettings(sampleData);
                    }
                    csvSettingsUsed = csvSettings;
                    return ParseCsvTextFile(filePath, csvSettings,hasHeaderActual);
                }
            }
            catch (Exception e)
            {
                if (!ifSilent)
                {
                    MessageBox.Show(string.Format("ERROR: DataImport::ParseCsvTextFile: {0}", e.Message),
                        "RockWisePreprocessor", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return null;
            }
        }

        public static DataTable TrimDataTable(DataTable dt)
        {
            //delect empty rows
            int i = 0;
            while (i < dt.Rows.Count)
            {
                bool delete = true;
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    if (dt.Rows[i][j].ToString().Trim() != string.Empty)
                    {
                        delete = false;
                        break;
                    }
                }
                if (delete)
                {
                    dt.Rows.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }

            //delete empty columns
            i = 0;
            while (i < dt.Columns.Count)
            {
                bool delete = true;
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (dt.Rows[j][i].ToString().Trim() != string.Empty)
                    {
                        delete = false;
                        break;
                    }
                }
                if (delete)
                {
                    dt.Columns.RemoveAt(i);
                }
                else
                {
                    i++;
                }
            }

            return dt;
        }
    }
}
