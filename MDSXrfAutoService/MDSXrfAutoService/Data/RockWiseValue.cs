﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XrfWindowsService
{
    [Serializable]
    public class RockWiseValue : IComparable
    {
        private bool isBQL;
        private double? normValue;
        private int? bqlFactor;
        private double? rawRead;

        public RockWiseValue()
        {
            isBQL=false;
            bqlFactor=(int?)null;
            rawRead=(double?)null;
            normValue=(double?)null;
            ShowLessThan = false;
        }

        public RockWiseValue(double value):this()
        {
            rawRead = value;
        }

        public RockWiseValue(double value, bool bql, int? factor,bool showLessThanSym):this()
        {
            rawRead = value;
            isBQL = bql;
            bqlFactor = factor;
            ShowLessThan = showLessThanSym;
        }

        public override string ToString()
        {
            if (ShowLessThan && isBQL)
                return "<" + (NormValue.HasValue? string.Format("{0:0.0}", Math.Round(NormValue.Value,1,MidpointRounding.AwayFromZero)):NormValue.ToString());
            else
                return NormValue.ToString();
        }

        public double? NormValue
        {
            get
            {
                if (isBQL)
                    normValue = bqlFactor.HasValue ? rawRead.HasValue? rawRead.Value / bqlFactor.Value : (double?)null:(double?)null;
                else
                    normValue = rawRead;

                return normValue;
            }
        }

        public double? RawData
        {
            set
            {
                rawRead = value.HasValue ? value.Value>0? value.Value: 0: (double?)null;
            }
        }

        public bool BQL
        {
            get
            {
                return isBQL;
            }
            set
            {
                isBQL = value;
            }
        }

        public bool ShowLessThan
        {
            get; set;
        }

        public int? BQLFactor
        {
            get
            {
                return bqlFactor;
            }
            set
            {
                bqlFactor = value;
            }
        }

        #region IComparable Members
            public int CompareTo(object obj)
            {
                if (obj is RockWiseValue)
                {
                    RockWiseValue p2 = (RockWiseValue)obj;
                    if (NormValue.HasValue && p2.NormValue.HasValue)
                        return NormValue.Value.CompareTo(p2.NormValue.Value);
                    else if (NormValue.HasValue)
                        return 1;
                    else if (p2.NormValue.HasValue)
                        return -1;
                    else
                        return 0;
                }
                else
                    throw new ArgumentException("Object is not a RockWiseValue.");
            }
            #endregion
    }
}
