﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;

namespace XrfWindowsService
{
    public class RockWiseCalibrationData : RockWiseCommon,ICalibrationData
    {
        public bool Select { get; set; }
        public string ProfileNameFromOrigFile { get; set; }
        public string Well { get; set; }
        public string Method { get; set; }
        public DateTime EvalTime { get; set; }
        public RockWiseProfileData ProfileAssigned { get; set; }
        public CalibrationSet ElementsCalibrationSet { get; set; }

        public RockWiseCalibrationData()
        {
        }

        public RockWiseCalibrationData(DataRow dr, int? lessThanValueFactor, bool showLessThanSym, CultureInfo ci)
        {
            Select = true;
            ProfileNameFromOrigFile = dr.Field<string>(0);
            Well = dr.Field<string>(1);
            Method = dr.Field<string>(2);
            EvalTime = DateTime.Parse(dr.Field<string>(3),ci.DateTimeFormat);

            for (int i = 4; i < dr.Table.Columns.Count; i++)
            {
                double outd;
                string tempstr = dr.Field<string>(i);

                if (String.IsNullOrEmpty(tempstr))
                    Elements[i - 4] = new RockWiseValue();
                else if (tempstr.Contains("<"))
                    Elements[i - 4] = new RockWiseValue(double.TryParse(tempstr.Substring(tempstr.IndexOf("<") + 1), out outd) ? outd : 0, true, lessThanValueFactor,showLessThanSym);
                else
                    Elements[i - 4] = new RockWiseValue(double.TryParse(tempstr, out outd) ? outd : 0);
            }
        }
    }
}
