﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace XrfWindowsService
{
    public enum ElementMolesEnum
    {
        Si,
        Ti,
        Al,
        Fe,
        Mn,
        Mg,
        Ca,
        Na,
        K,
        P,
        S,
        Cl,
        Ba
    }
}
