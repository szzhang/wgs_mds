﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;

namespace XrfWindowsService
{
    [Serializable]
    public class Calibration
    {
        public string Name { get; set; }
        public int NumSamples { get; set; }
        public double? Min { get; set; }
        public double? Max { get; set; }
        public double? Mean { get; set; }
        public double? Median { get; set; }
        public double? StdDev { get; set; }
        public double? Rsd
        {
            get
            {
                if (StdDev.HasValue && Mean.HasValue)
                    return 100 * StdDev.Value / Mean.Value;
                else
                    return null;
            }
        }
        public double? AcceptedValue { get; set; }
        public double? AcceptedValueMod { get; set; }
        public double? Diff
        {
            get
            {
                if (Mean.HasValue)
                {
                    if (AcceptedValueMod.HasValue)
                        return Mean.Value - AcceptedValueMod.Value;
                    else if (AcceptedValue.HasValue)
                        return Mean.Value - AcceptedValue.Value;
                }
                return null;
            }
        }
        public double? DiffPercent
        {
            get
            {
                if (Diff.HasValue)
                {
                    if (AcceptedValueMod.HasValue)
                        return Diff.Value / AcceptedValueMod.Value;
                    else if (AcceptedValue.HasValue)
                        return Diff.Value / AcceptedValue.Value;
                }
                return null;
            }
        }
        public double? CorrFactorCalculated
        {
            get
            {
                if (Mean.HasValue)
                {
                    if (AcceptedValueMod.HasValue)
                        return AcceptedValueMod.Value * 100 / Mean.Value - 100;
                    else if (AcceptedValue.HasValue)
                        return AcceptedValue.Value * 100 / Mean.Value - 100;
                }
                return null;
            }
        }
        double? _cfFinal;
        public double? CorrFactorFinal
        {
            get
            {
                if (_cfFinal.HasValue)
                    return _cfFinal.Value;
                else
                    return CorrFactorCalculated;
            }
            set
            {
                _cfFinal = value;
            }
        }

        public Calibration(string name, double? corrValue)
        {
            Name = name;
            CorrFactorFinal = corrValue;
        }

        public Calibration(string name, List<double?> std, double? acceptedValue)
        {
            Name = name;
            NumSamples = std.Where(c => c.HasValue).ToList().Count;
            Min = std.Min();
            Max = std.Max();
            Mean = std.Average();
            Median = std.Median();
            StdDev = std.StdDev();
            AcceptedValue = acceptedValue;
        }

        public void ChangePoints(List<double?> std)
        {
            NumSamples = std.Where(c => c.HasValue).ToList().Count;
            Min = std.Min();
            Max = std.Max();
            Mean = std.Average();
            Median = std.Median();
            StdDev = std.StdDev();
        }
    }

    [Serializable]
    public class CalibrationSet
    {
        private Calibration[] calis;

        private string profileName;

        public CalibrationSet()
        {
            calis = new Calibration[Enum.GetNames(typeof(ElementNamesEnum)).Count()];
            profileName = "";
        }

        public CalibrationSet(List<RockWiseCalibrationData> listStd, RockWiseProfileData acceptedValues):this()
        {
            for (int i = 0; i < Enum.GetNames(typeof(ElementNamesEnum)).Count(); i++)
            {
                calis[i]= new Calibration(((ElementNamesEnum)i).ToString(), listStd.Select(c => c.Elements[i].NormValue).ToList(), acceptedValues.Elements[i].NormValue);
            }
            profileName = acceptedValues.ProfileName;
        }

        public void ChangeCalibrationSetPoints(List<RockWiseCalibrationData> listStd)
        {
            for (int i = 0; i < Enum.GetNames(typeof(ElementNamesEnum)).Count(); i++)
            {
                calis[i].ChangePoints(listStd.Select(c => c.Elements[i].NormValue).ToList());
            }
        }

        public int Length
        {
            get { return calis.Length; }
        }

        public string ProfileName
        {
            get { return profileName; }
        }

        public Calibration this[int index]
        {
            get
            {
                return calis[index];
            }

            set
            {
                calis[index] = value;
            }
        }

        public List<Calibration> ToList()
        {
            return calis.ToList();
        }

        public DataTable ToDataTable()
        {
            DataTable dt = new DataTable();

            foreach (PropertyInfo info in typeof(Calibration).GetProperties())
            {
                if (info.Name=="Name")
                    dt.Columns.Add(new DataColumn(info.Name, typeof(string)));
                else
                    dt.Columns.Add(new DataColumn(info.Name, typeof(double)));
            }
            foreach (Calibration t in calis)
            {
                DataRow row = dt.NewRow();
                foreach (PropertyInfo info in typeof(Calibration).GetProperties())
                {
                    if (info.GetValue(t, null) == null)
                        row[info.Name] = DBNull.Value;
                    else
                        row[info.Name] = info.GetValue(t, null);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        public static CalibrationSet AverageCalibrationSetPoints(List<RockWiseCalibrationData> listStdPoints)
        {
            //List<RockWiseProfileData> profilesRedefined = listStdPoints.Where(o => o.ProfileAssigned != null).Select(s => s.ProfileAssigned).Distinct().ToList();

            //foreach (RockWiseProfileData f in profilesRedefined)
            //{
            //    CalibrationSet ca = new CalibrationSet(listStdPoints.Where(s => s.ProfileAssigned == f).ToList(), f);
            //    listStdPoints.Where(s => s.ProfileAssigned == f).ToList().ForEach(a => a.ElementsCalibrationSet = ca);
            //}

            CalibrationSet ca1 = new CalibrationSet();
            for (int i = 0; i < Enum.GetNames(typeof(ElementNamesEnum)).Count(); i++)
            {
                ca1[i] = new Calibration(((ElementNamesEnum)i).ToString(), listStdPoints.Select(s => s.ElementsCalibrationSet).Distinct().Select(o => o[i].CorrFactorFinal).Average());
            }
            return ca1;
        }


    }

}
