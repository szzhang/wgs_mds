﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;

namespace XrfWindowsService
{
    public class RockWiseProfileData : RockWiseCommon, IProfileData
    {
        public string ProfileName { get; set; }

        public RockWiseProfileData()
        {
        }

        public RockWiseProfileData(DataRow dr, int? lessThanValueFactor, bool showLessThanSym, CultureInfo ci)
        {
            ProfileName = dr.Field<string>(0);

            for (int i = 1; i < dr.Table.Columns.Count; i++)
            {
                double outd;
                string tempstr = dr.Field<string>(i);
                if (Enum.GetNames(typeof(ElementNamesEnum)).Where(c => c.ToLower() == dr.Table.Columns[i].ColumnName.ToLower()).Count() > 0)
                {
                    ElementNamesEnum elem = (ElementNamesEnum)Enum.Parse(typeof(ElementNamesEnum), Enum.GetNames(typeof(ElementNamesEnum)).Where(c => c.ToLower() == dr.Table.Columns[i].ColumnName.ToLower()).First().ToString());
                    if (String.IsNullOrEmpty(tempstr))
                        Elements[(int)elem] = new RockWiseValue();
                    else if (tempstr.Contains("<"))
                        Elements[(int)elem] = new RockWiseValue(double.TryParse(tempstr.Substring(tempstr.IndexOf("<") + 1), out outd) ? outd : 0, true, lessThanValueFactor,showLessThanSym);
                    else
                        Elements[(int)elem] = new RockWiseValue(double.TryParse(tempstr, out outd) ? outd : 0);
                }
            }
        }
    }
}
