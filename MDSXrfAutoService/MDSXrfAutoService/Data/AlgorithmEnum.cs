﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace XrfWindowsService
{
    public enum AlgorithmEnum
    {
        [Description("use equally divided time blocks")]
        TimeBlock,
        [Description("choose closest fixed number of standard points for each measured point")]
        SampleNumber,
        [Description("use manually specified time blocks")]
        ManualTimeBlock
    }
}
