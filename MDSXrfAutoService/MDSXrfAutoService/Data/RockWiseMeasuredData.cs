﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Text.RegularExpressions;

namespace XrfWindowsService
{
    [Serializable]
    public class RockWiseMeasuredData : RockWiseCommon, IMeasuredData
    {
        public bool Select { get; set; }
        public double Depth { get; set; }
        public string Well { get; set; }
        public string Method { get; set; }
        public DateTime EvalTime { get; set; }

        [NonSerialized]
        private RockWiseProfileData profileA;
        public RockWiseProfileData ProfileAssigned
        {
            get { return profileA; }
            set { profileA = value; }
        }

        [NonSerialized]
        private CalibrationSet cals;         
        public CalibrationSet ElementsCalibrationSet
        {
            get { return cals; }
            set { cals = value; }
        }
        public RockWiseMeasuredData()
        {
        }

        public RockWiseMeasuredData(DataRow dr, int? lessThanValueFactor, bool showLessThanSym, CultureInfo ci)
        {
            Select = true;
            string pnum = @"^\d*\.?\d*";
            Match m = Regex.Match(dr.Field<string>(0), pnum);
            double dnumber;
            Depth = double.TryParse(m.Value, out dnumber) ? Math.Round(dnumber,2) : 0;

            Well = dr.Field<string>(1);
            Method = dr.Field<string>(2);
            EvalTime = DateTime.Parse(dr.Field<string>(3),ci.DateTimeFormat);

            for (int i = 4; i < dr.Table.Columns.Count; i++)
            {
                double outd;
                string tempstr = dr.Field<string>(i);
                if (String.IsNullOrEmpty(tempstr))
                    Elements[i-4]=new RockWiseValue();
                else if (tempstr.Contains("<"))
                    Elements[i-4]=new RockWiseValue(double.TryParse(tempstr.Substring(tempstr.IndexOf("<") + 1),out outd)?outd:0,true,lessThanValueFactor,showLessThanSym);
                else
                    Elements[i - 4] = new RockWiseValue(double.TryParse(tempstr, out outd) ? outd : 0);
            }
        }
    }
}
