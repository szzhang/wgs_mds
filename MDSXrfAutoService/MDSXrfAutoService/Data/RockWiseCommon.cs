﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;

namespace XrfWindowsService
{
    [Serializable]
    public class RockWiseCommon
    {
        [Serializable]
        public class ElementsIndexer
        {
            protected RockWiseValue[] elements;

            public ElementsIndexer(ref RockWiseValue[] arrayOwnerElements)
            {
                this.elements = arrayOwnerElements;
            }

            public RockWiseValue this[int index]
            {
                get 
                {
                    return elements[index]; 
                }
                set 
                {
                    elements[index] = value;
                }
            }

            public RockWiseValue Totals
            {
                get
                {
                    double sum = 0;
                    if (elements[(int)ElementNamesEnum.SiO2].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.SiO2].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.TiO2].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.TiO2].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Al2O3].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Al2O3].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Fe2O3].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Fe2O3].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.MnO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.MnO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.MgO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.MgO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.CaO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.CaO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Na2O].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Na2O].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.K2O].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.K2O].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.P2O5].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.P2O5].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.SO3].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.SO3].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Cl].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Cl].NormValue.Value;
                    return new RockWiseValue(sum);
                }
            }

            public RockWiseValue S
            {
                get
                {
                    if (elements[(int)ElementNamesEnum.SO3].NormValue.HasValue)
                        return new RockWiseValue(elements[(int)ElementNamesEnum.SO3].NormValue.Value * SO3_S);
                    else
                        return new RockWiseValue();
                }
            }

            public RockWiseValue TotalSFromSO3
            {
                get
                {
                    double sum = 0;
                    if (elements[(int)ElementNamesEnum.SiO2].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.SiO2].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.TiO2].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.TiO2].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Al2O3].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Al2O3].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Fe2O3].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Fe2O3].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.MnO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.MnO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.MgO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.MgO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.CaO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.CaO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Na2O].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Na2O].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.K2O].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.K2O].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.P2O5].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.P2O5].NormValue.Value;
                    if (S.NormValue.HasValue) sum += S.NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Cl].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Cl].NormValue.Value;
                    return new RockWiseValue(sum);
                }
            }

            public RockWiseValue TotalS_CaCO3
            {
                get
                {
                    double sum = 0;
                    if (elements[(int)ElementNamesEnum.SiO2].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.SiO2].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.TiO2].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.TiO2].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Al2O3].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Al2O3].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Fe2O3].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Fe2O3].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.MnO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.MnO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.MgO].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.MgO].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Na2O].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Na2O].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.K2O].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.K2O].NormValue.Value;
                    if (elements[(int)ElementNamesEnum.P2O5].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.P2O5].NormValue.Value;
                    if (S.NormValue.HasValue) sum += S.NormValue.Value;
                    if (elements[(int)ElementNamesEnum.Cl].NormValue.HasValue) sum += elements[(int)ElementNamesEnum.Cl].NormValue.Value;
                    if (CaCO3.NormValue.HasValue) sum += CaCO3.NormValue.Value;
                    return new RockWiseValue(sum);
                }
            }

            public RockWiseValue CaCO3
            {
                get
                {
                    if (elements[(int)ElementNamesEnum.CaO].NormValue.HasValue)
                        return new RockWiseValue(elements[(int)ElementNamesEnum.CaO].NormValue.Value / CaO_CaCO3);
                    else
                        return new RockWiseValue();
                }
            }
        }

        [Serializable]
        public class ElementsLeftIndexer : ElementsIndexer
        {
            protected double?[] molesLeft;

            public ElementsLeftIndexer(ref double?[] moles, ref RockWiseValue[] elementsLeft)
                : base(ref elementsLeft)
            {
                this.molesLeft = moles;
            }

            public new RockWiseValue this[int index]
            {
                get
                {
                    if (index == (int)ElementNamesEnum.Na2O)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Na].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Na].Value*Na_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.MgO)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Mg].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Mg].Value*Mg_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.Al2O3)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Al].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Al].Value*Al_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.SiO2)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Si].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Si].Value*Si_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.P2O5)
                    {
                        if (molesLeft[(int)ElementMolesEnum.P].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.P].Value*P_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.SO3)
                    {
                        if (molesLeft[(int)ElementMolesEnum.S].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.S].Value*S_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.Cl)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Cl].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Cl].Value*Cl_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.K2O)
                    {
                        if (molesLeft[(int)ElementMolesEnum.K].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.K].Value*K_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.CaO)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Ca].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Ca].Value*Ca_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.TiO2)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Ti].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Ti].Value*Ti_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.Fe2O3)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Fe].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Fe].Value*Fe_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.MnO)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Mn].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Mn].Value*Mn_Mwt);
                        else
                            elements[index] = new RockWiseValue();
                    }
                    else if (index == (int)ElementNamesEnum.Ba)
                    {
                        if (molesLeft[(int)ElementMolesEnum.Ba].HasValue)
                            elements[index] = new RockWiseValue(molesLeft[(int)ElementMolesEnum.Ba].Value*Ba_Mwt*10000/1.1165);
                        else
                            elements[index] = new RockWiseValue();
                    }

                    return elements[index];
                }
            }
        }

        [Serializable]
        public class ElementsReversedIndexer : ElementsIndexer
        {
            protected List<WFTMineral> minerals;

            public ElementsReversedIndexer(ref List<WFTMineral> mineralsElementsCaculated, ref RockWiseValue[] elementsReversed)
                : base(ref elementsReversed)
            {
                this.minerals = mineralsElementsCaculated;
            }

            public new RockWiseValue this[int index]
            {
                get
                {
                    elements[index] = new RockWiseValue(minerals.Sum(m => m.Oxides.Where(o => o.Name == (ElementNamesEnum)index).ToList().Sum(c => c.WtPercent)));
                    return elements[index];
                }
            }
        }

        [Serializable]
        public class MoleElementsIndexer
        {
            protected double?[] molesCalculated;
            protected RockWiseValue[] elements;

            public MoleElementsIndexer(ref double?[] moles, ref RockWiseValue[] eles)
            {
                this.molesCalculated = moles;
                this.elements = eles;
            }

            public double? this[int index]
            {
                get
                {
                    if (index == (int)ElementMolesEnum.Si)
                    {
                        if (elements[(int)ElementNamesEnum.SiO2].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.SiO2].NormValue.Value/Si_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Ti)
                    {
                        if (elements[(int)ElementNamesEnum.TiO2].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.TiO2].NormValue.Value / Ti_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Al)
                    {
                        if (elements[(int)ElementNamesEnum.Al2O3].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.Al2O3].NormValue.Value / Al_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Fe)
                    {
                        if (elements[(int)ElementNamesEnum.Fe2O3].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.Fe2O3].NormValue.Value / Fe_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Mn)
                    {
                        if (elements[(int)ElementNamesEnum.MnO].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.MnO].NormValue.Value / Mn_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Mg)
                    {
                        if (elements[(int)ElementNamesEnum.MgO].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.MgO].NormValue.Value / Mg_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Ca)
                    {
                        if (elements[(int)ElementNamesEnum.CaO].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.CaO].NormValue.Value / Ca_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Na)
                    {
                        if (elements[(int)ElementNamesEnum.Na2O].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.Na2O].NormValue.Value /Na_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.K)
                    {
                        if (elements[(int)ElementNamesEnum.K2O].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.K2O].NormValue.Value / K_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.P)
                    {
                        if (elements[(int)ElementNamesEnum.P2O5].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.P2O5].NormValue.Value /P_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.S)
                    {
                        if (elements[(int)ElementNamesEnum.SO3].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.SO3].NormValue.Value /S_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Cl)
                    {
                        if (elements[(int)ElementNamesEnum.Cl].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.Cl].NormValue.Value / Cl_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }
                    else if (index == (int)ElementMolesEnum.Ba)
                    {
                        if (elements[(int)ElementNamesEnum.Ba].NormValue.HasValue)
                            molesCalculated[index] = elements[(int)ElementNamesEnum.Ba].NormValue.Value * 1.1165 / 10000 / Ba_Mwt;
                        else
                            molesCalculated[index] = (double?)null;
                    }

                    return molesCalculated[index];
                }
            }

            public int Length
            {
                get { return molesCalculated.Length; }
            }
        }

        [Serializable]
        public class MoleLeftIndexer
        {
            private RockWiseCommon arrayOwner;

            public MoleLeftIndexer(RockWiseCommon arrayOwner)
            {
                this.arrayOwner = arrayOwner;
            }

            public double? this[int index]
            {
                set
                {
                    if (value.HasValue)
                        if (value.Value > 0)
                            arrayOwner.molesLeft[index] = value;
                        else
                            arrayOwner.molesLeft[index] = 0;
                    else
                        arrayOwner.molesLeft[index] = value;
                }
            }

            public int Length
            {
                get { return arrayOwner.molesLeft.Length; }
            }
        }

        private ElementsReversedIndexer arrayElementsReversed;
        private ElementsIndexer arrayElements;
        private MoleElementsIndexer arrayMoleElements;
        private MoleLeftIndexer arrayMoleLeft;
        private ElementsLeftIndexer arrayElementsLeft;
        private RockWiseValue[] elements;
        private double?[] molesCalculated;
        private double?[] molesLeft;
        private RockWiseValue[] elementsLeft;
        private RockWiseValue[] elementsReversed;
        
        private const double SO3_S = 0.4005;
        private const double CaO_CaCO3 = 0.5603;

        private const double Si_Mwt = 60.0843;
        private const double Ti_Mwt = 79.8658;
        private const double Al_Mwt = 101.9613/2;
        private const double Fe_Mwt = 159.6882/2;
        private const double Mn_Mwt = 70.9375;
        private const double Mg_Mwt = 40.3044;
        private const double Ca_Mwt = 56.0774;
        private const double Na_Mwt = 61.9789/2;
        private const double K_Mwt = 94.196/2;
        private const double P_Mwt = 141.9445/2;
        private const double S_Mwt = 80.0642;
        private const double Cl_Mwt = 35.4527;
        private const double Ba_Mwt = 153.3264;

        public RockWiseCommon()
        {
            elements = new RockWiseValue[Enum.GetNames(typeof(ElementNamesEnum)).Count()];
            elementsLeft = new RockWiseValue[Enum.GetNames(typeof(ElementNamesEnum)).Count()];
            elementsReversed = new RockWiseValue[Enum.GetNames(typeof(ElementNamesEnum)).Count()];
            for (int i = 0; i < Enum.GetNames(typeof(ElementNamesEnum)).Count(); i++)
            {
                elements[i] = new RockWiseValue();
                elementsLeft[i] = new RockWiseValue();
                elementsReversed[i] = new RockWiseValue();
            }
            molesCalculated = new double?[Enum.GetNames(typeof(ElementMolesEnum)).Count()];
            molesLeft = new double?[Enum.GetNames(typeof(ElementMolesEnum)).Count()];
            arrayElements = new ElementsIndexer(ref this.elements);
            arrayMoleElements = new MoleElementsIndexer(ref this.molesCalculated, ref this.elements);
            arrayMoleLeft = new MoleLeftIndexer(this);
            arrayElementsLeft = new ElementsLeftIndexer(ref this.molesLeft, ref this.elementsLeft);
            arrayElementsReversed = new ElementsReversedIndexer(ref mineralsElementsCaculated, ref this.elementsReversed);
        }

        public ElementsIndexer Elements
        {
            get { return arrayElements; }
        }

        public ElementsReversedIndexer ElementsReversed
        {
            get { return arrayElementsReversed; }
        }

        public MoleElementsIndexer MoleElements
        {
            get { return arrayMoleElements; }
        }

        public MoleLeftIndexer MoleElementsLeft
        {
            get { return arrayMoleLeft; }
        }

        public ElementsLeftIndexer ElementsLeft
        {
            get { return arrayElementsLeft; }
        }

        private List<WFTMineral> mineralsElementsCaculated;
        public List<WFTMineral> MineralsElementsCaculated 
        {
            get
            {
                return mineralsElementsCaculated;
            }
            set
            {
                mineralsElementsCaculated=value;
                arrayElementsReversed = new ElementsReversedIndexer(ref mineralsElementsCaculated, ref this.elementsReversed);
            }
        }

        public double MineralElementsTotalPercent
        {
            get
            {
                return MineralsElementsCaculated.Sum(m => m.WtPercent);
            }
        }

        public double GetMineralElementsCategoryTotalPercent (string cat)
        {
            if (!String.IsNullOrEmpty(cat))
                return MineralsElementsCaculated.Where(m => m.Category == cat).Sum(m => m.WtPercent);
            else
                return MineralsElementsCaculated.Where(m => String.IsNullOrEmpty(m.Category)).Sum(m => m.WtPercent);
        }

        public void PerformCorrection(RockWiseCorrectionMask mask, CalibrationSet calSet)
        {
            foreach (DataColumn column in mask.MECorrectionMask.Columns)
            {
                if ((bool)mask.MECorrectionMask.Rows[0][column])
                {
                    int i = (int)Enum.GetValues(typeof(ElementNamesEnum)).Cast<ElementNamesEnum>().Where(c => c.ToString() == column.ColumnName).First();
                    if (Elements[i].NormValue.HasValue && calSet[i].CorrFactorFinal.HasValue && !Elements[i].BQL)
                        Elements[i].RawData = Elements[i].NormValue.Value + Elements[i].NormValue.Value / 100 * calSet[i].CorrFactorFinal;
                }
            }
            foreach (DataColumn column in mask.TECorrectionMask.Columns)
            {
                if ((bool)mask.TECorrectionMask.Rows[0][column])
                {
                    int i = (int)Enum.GetValues(typeof(ElementNamesEnum)).Cast<ElementNamesEnum>().Where(c => c.ToString() == column.ColumnName).First();
                    if (Elements[i].NormValue.HasValue && calSet[i].CorrFactorFinal.HasValue && !Elements[i].BQL)
                        Elements[i].RawData = Elements[i].NormValue.Value + Elements[i].NormValue.Value / 100 * calSet[i].CorrFactorFinal;
                }
            }
        }

        public void PerformMineralCalculation(List<WFTMineral> minerals)
        {
            double?[] molesLeftTemp = new double?[Enum.GetNames(typeof(ElementMolesEnum)).Count()];
            for (int j = 0; j < arrayMoleElements.Length; j++)
                molesLeftTemp[j] = arrayMoleElements[j];
            MineralsElementsCaculated=minerals.Select(item => item.Clone()).ToList();

            for (int i = 0; i < MineralsElementsCaculated.Count; i++)
            {
                for (int j = 0; j < arrayMoleElements.Length; j++)
                    MoleElementsLeft[j] = molesLeftTemp[j];

                foreach (MineralElement e in MineralsElementsCaculated[i].Elements)
                    e.MoleValue = molesLeftTemp[(int)e.Name].HasValue? molesLeftTemp[(int)e.Name].Value:0;

                MineralsElementsCaculated[i].WtPercent = MineralsElementsCaculated[i].Moles * MineralsElementsCaculated[i].WtPerMole;
                foreach (MineralElement e in MineralsElementsCaculated[i].Elements)
                {
                    if (molesLeftTemp[(int)e.Name].HasValue)
                    {
                        //if ((molesLeftTemp[(int)e.Name].Value - (MineralsElementsCaculated[i].Moles * e.Factor)) >= 0)
                        //{
                            molesLeftTemp[(int)e.Name] = Math.Round(molesLeftTemp[(int)e.Name].Value - (MineralsElementsCaculated[i].Moles * e.Factor),8);
                            continue;
                        //}
                    }
                            
                    //MineralsCaculated.RemoveAt(i);
                    //i--;
                    MineralsElementsCaculated[i].WtPercent = 0;
                    for (int j = 0; j < arrayMoleElements.Length; j++)
                        molesLeftTemp[j] = molesLeft[j];
                    break;
                }
            }
            for (int j = 0; j < arrayMoleElements.Length; j++)
                MoleElementsLeft[j] = molesLeftTemp[j];
        }

        public RockWiseCommon(DataRow dr, int? lessThanValueFactor, bool showLessThanSym, CultureInfo ci):this()
        {
        }
    }
}
