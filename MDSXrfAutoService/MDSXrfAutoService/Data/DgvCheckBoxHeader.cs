﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace XrfWindowsService
{
    public delegate void CheckBoxClickedHandler(object sender, DataGridViewCheckBoxHeaderCellEventArgs state);

    public class DataGridViewCheckBoxHeaderCellEventArgs : EventArgs
    {

        public DataGridViewCheckBoxHeaderCellEventArgs(bool bChecked, int index)
        {
            Checked = bChecked;
            ColumnIndex = index;
        }

        public bool Checked { get; private set; }

        public int ColumnIndex { get; private set; }

    }

    class DatagridViewCheckBoxHeaderCell : DataGridViewColumnHeaderCell
    {
        Point checkBoxLocation;
        Size checkBoxSize;
        bool _checked = false;
        Point _cellLocation = new Point();
        System.Windows.Forms.VisualStyles.CheckBoxState _cbState =
        System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal;
        public event CheckBoxClickedHandler OnCheckBoxClicked;

        public DatagridViewCheckBoxHeaderCell()
        {
        }

        protected override void Paint(System.Drawing.Graphics graphics,
                                    System.Drawing.Rectangle clipBounds,
                                    System.Drawing.Rectangle cellBounds,
                                    int rowIndex,
                                    DataGridViewElementStates dataGridViewElementState,
                                    object value,
                                    object formattedValue,
                                    string errorText,
                                    DataGridViewCellStyle cellStyle,
                                    DataGridViewAdvancedBorderStyle advancedBorderStyle,
                                    DataGridViewPaintParts paintParts)
        {
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value, formattedValue, errorText, cellStyle, advancedBorderStyle, paintParts);
            Point p = new Point();
            Size s = CheckBoxRenderer.GetGlyphSize(graphics, System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal);
            p.X = cellBounds.Location.X + cellBounds.Width - s.Width;
            p.Y = cellBounds.Location.Y + (cellBounds.Height / 2) - (s.Height / 2);
            _cellLocation = cellBounds.Location;
            checkBoxLocation = p;
            checkBoxSize = s;

            if (_checked)
                _cbState = System.Windows.Forms.VisualStyles.CheckBoxState.CheckedNormal;
            else
                _cbState = System.Windows.Forms.VisualStyles.CheckBoxState.UncheckedNormal;
            CheckBoxRenderer.DrawCheckBox(graphics, checkBoxLocation, _cbState);
        }

        protected override void OnMouseClick(DataGridViewCellMouseEventArgs e)
        {
            Point p = new Point(e.X + _cellLocation.X, e.Y + _cellLocation.Y);
            if (p.X >= checkBoxLocation.X && p.X <= checkBoxLocation.X + checkBoxSize.Width && p.Y >= checkBoxLocation.Y && p.Y <= checkBoxLocation.Y + checkBoxSize.Height)
            {
                _checked = !_checked;
                if (OnCheckBoxClicked != null)
                {
                    OnCheckBoxClicked(this.DataGridView, new DataGridViewCheckBoxHeaderCellEventArgs(_checked, this.ColumnIndex));
                    this.DataGridView.InvalidateCell(this);
                }
            }
            base.OnMouseClick(e);
        }

        public override object Clone()
        {
            DatagridViewCheckBoxHeaderCell obj = (DatagridViewCheckBoxHeaderCell)base.Clone();
            obj.checkBoxLocation = checkBoxLocation;
            obj.checkBoxSize = checkBoxSize;
            obj._checked = _checked;
            obj._cellLocation = _cellLocation;
            obj._cbState = _cbState;
            obj.OnCheckBoxClicked = OnCheckBoxClicked;
            return obj;
        }

        public bool Checked
        {
            get
            {
                return _checked;
            }

            set 
            {
                _checked = value;
            }
        }
    }
}
