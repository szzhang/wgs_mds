﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace XrfWindowsService
{
    public enum ElementNamesEnum
    {
        [MinElemValue(0.25)]
        [MaxElemValue(2)]
        [Description("ME")]
        Na2O,
        [MaxElemValue(21)]
        [Description("ME")]
        MgO,
        [MaxElemValue(20)]
        [Description("ME")]
        Al2O3,
        [Description("ME")]
        SiO2,
        [MaxElemValue(2)]
        [Description("ME")]
        P2O5,
        [Description("ME")]
        SO3,
        [Description("ME")]
        Cl,
        [MaxElemValue(5)]
        [Description("ME")]
        K2O,
        [MaxElemValue(56)]
        [Description("ME")]
        CaO,
        [MaxElemValue(2)]
        [Description("ME")]
        TiO2,
        [Description("TE")]
        V,
        [Description("TE")]
        Cr,
        [MaxElemValue(1)]
        [Description("ME")]
        MnO,
        [MaxElemValue(10)]
        [Description("ME")]
        Fe2O3,
        [Description("TE")]
        Co,
        [Description("TE")]
        Ni,
        [Description("TE")]
        Cu,
        [Description("TE")]
        Zn,
        [Description("TE")]
        Ga,
        [Description("TE")]
        As,
        [Description("TE")]
        Br,
        [Description("TE")]
        Rb,
        [Description("TE")]
        Sr,
        [Description("TE")]
        Y,
        [Description("TE")]
        Zr,
        [Description("TE")]
        Nb,
        [Description("TE")]
        Mo,
        [Description("TE")]
        Cs,
        [Description("TE")]
        Ba,
        [Description("TE")]
        La,
        [Description("TE")]
        Ce,
        [Description("TE")]
        Hf,
        [Description("TE")]
        Ta,
        [Description("TE")]
        W,
        [Description("TE")]
        Pb,
        [Description("TE")]
        Th,
        [Description("TE")]
        U
    }

    public class MaxElemValueAttribute : Attribute
    {
        private readonly double mvalue;
        public MaxElemValueAttribute(double maxv) { this.mvalue = maxv; }

        public double Value
        {
            get
            {
                return mvalue;
            }
        }
    }

    public class MinElemValueAttribute : Attribute
    {
        private readonly double mvalue;
        public MinElemValueAttribute(double minv) { this.mvalue = minv; }

        public double Value
        {
            get
            {
                return mvalue;
            }
        }
    }
}
