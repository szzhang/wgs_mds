﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;

namespace XrfWindowsService
{
    public class RockWiseCorrectionMask
    {
        private DataTable mElements;
        private DataTable tElements;
 
        public RockWiseCorrectionMask()
        {
            mElements = new DataTable();
            tElements = new DataTable();
            foreach (ElementNamesEnum elem in Enum.GetValues(typeof(ElementNamesEnum)))
            {
                if (elem.GetAttributeOfType<DescriptionAttribute>().Description == "ME")
                    mElements.Columns.Add(elem.ToString(), typeof(bool));
                else if (elem.GetAttributeOfType<DescriptionAttribute>().Description == "TE")
                    tElements.Columns.Add(elem.ToString(), typeof(bool));
            }

            DataRow row1 = mElements.NewRow();
            DataRow row2 = tElements.NewRow();
            foreach (DataColumn column in mElements.Columns)
            {
                row1[column]=true;
            }
            foreach (DataColumn column in tElements.Columns)
            {
                row2[column] =false;
            }
            mElements.Rows.Add(row1);
            tElements.Rows.Add(row2);
        }

        public DataTable MECorrectionMask
        {
            get
            {
                return mElements;
            }
            set
            {
                mElements = value;
            }
        }

        public DataTable TECorrectionMask
        {
            get
            {
                return tElements;
            }
            set
            {
                tElements = value;
            }
        }
    }
}
