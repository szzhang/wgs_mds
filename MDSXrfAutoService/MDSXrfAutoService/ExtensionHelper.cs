﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Globalization;
using System.IO;

namespace XrfWindowsService
{
    public static class ExtensionHelper
    {
        public static double? Median<T>(this IEnumerable<T> source)
        {
            if (Nullable.GetUnderlyingType(typeof(T)) != null)
                source = source.Where(x => x != null);

            int count = source.Count();
            if (count == 0)
                return null;

            source = source.OrderBy(n => n);

            int midpoint = count / 2;
            if (count % 2 == 0)
                return (Convert.ToDouble(source.ElementAt(midpoint - 1)) + Convert.ToDouble(source.ElementAt(midpoint))) / 2.0;
            else
                return Convert.ToDouble(source.ElementAt(midpoint));
        }

        public static double? StdDev<T>(this IEnumerable<T> source)
        {
            if (Nullable.GetUnderlyingType(typeof(T)) != null)
                source = source.Where(x => x != null);

            int count = source.Count();
            if (count == 0)
                return null;

            double avg = source.Select(c=>Convert.ToDouble(c)).Average();
            return Math.Sqrt(source.Select(c => Convert.ToDouble(c)).Average(v => Math.Pow(v - avg, 2)));
        }

        public static DataTable ToElemDataTable<T>(this IList<T> data, bool reversed, string prefix, string suffix, bool showProfile) where T : RockWiseCommon
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                if (prop.Name == "ProfileAssigned")
                {
                    if (showProfile) table.Columns.Add(prop.Name, typeof(string));
                }
                else if (!prop.Name.Contains("Elements"))
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            for (int i = 0; i < Enum.GetNames(typeof(ElementNamesEnum)).Count(); i++)
            {
                table.Columns.Add(prefix + ((ElementNamesEnum)i).ToString() + suffix, typeof(RockWiseValue));
            }
            table.Columns.Add(prefix + "S" + suffix, typeof(RockWiseValue));
            table.Columns.Add(prefix + "CaCO3" + suffix, typeof(RockWiseValue));
            table.Columns.Add(prefix + "TotalSFromSO3" + suffix, typeof(RockWiseValue));
            table.Columns.Add(prefix + "TotalS_CaCO3" + suffix, typeof(RockWiseValue));
            table.Columns.Add(prefix + "Totals" + suffix, typeof(RockWiseValue));

            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (prop.Name == "ProfileAssigned")
                    {
                        if (showProfile) row[prop.Name] = (RockWiseProfileData)prop.GetValue(item) != null ? ((RockWiseProfileData)prop.GetValue(item)).ProfileName : ""; 
                    }
                    else if (!prop.Name.Contains("Elements"))
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }
                for (int i = 0; i < Enum.GetNames(typeof(ElementNamesEnum)).Count(); i++)
                {
                    row.SetField(prefix + ((ElementNamesEnum)i).ToString() + suffix, reversed ? item.ElementsReversed[i]: item.Elements[i]);
                }
                row.SetField(prefix + "S" + suffix,reversed ? item.ElementsReversed.S: item.Elements.S);
                row.SetField(prefix + "CaCO3" + suffix,reversed ? item.ElementsReversed.CaCO3: item.Elements.CaCO3);
                row.SetField(prefix + "TotalSFromSO3" + suffix, reversed ? (object)item.ElementsReversed.TotalSFromSO3 : item.Elements.TotalSFromSO3);
                row.SetField(prefix + "TotalS_CaCO3" + suffix,reversed ? (object)item.ElementsReversed.TotalS_CaCO3: item.Elements.TotalS_CaCO3);
                row.SetField(prefix + "Totals" + suffix, reversed ? (object)item.ElementsReversed.Totals : item.Elements.Totals);

                table.Rows.Add(row);
            }
            return table;
        }

        public static DataTable ToMineralsDataTable<T>(this IList<T> data, List<WFTMineral> minerals, bool norm, string excessPrefix, string excessSuffix) where T : RockWiseMeasuredData
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
            {
                if (!prop.Name.Contains("Elements") && prop.Name != "ProfileAssigned")
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            for (int i = 0; i <minerals.Count; i++)
            {
                table.Columns.Add(minerals[i].OutputName != null ? minerals[i].OutputName : minerals[i].Name, typeof(double));
            }

            foreach (string cat in minerals.Where(m=>!String.IsNullOrEmpty(m.Category)).GroupBy(m => m.Category).Select(g => g.First().Category))
            {
                table.Columns.Add(cat, typeof(double));
            }

            //table.Columns.Add("Other", typeof(double));
            table.Columns.Add("Mineral Total", typeof(double));

            if (data.Any(o=>o.ElementsLeft[(int)ElementNamesEnum.Na2O].NormValue.HasValue?o.ElementsLeft[(int)ElementNamesEnum.Na2O].NormValue.Value>0:false))
                table.Columns.Add(excessPrefix + "Na2O" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.MgO].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.MgO].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "MgO" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.Al2O3].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.Al2O3].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "Al2O3" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.SiO2].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.SiO2].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "SiO2" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.P2O5].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.P2O5].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "P2O5" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.SO3].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.SO3].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "SO3" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.Cl].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.Cl].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "Cl" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.K2O].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.K2O].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "K2O" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.CaO].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.CaO].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "CaO" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.TiO2].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.TiO2].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "TiO2" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.Fe2O3].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.Fe2O3].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "Fe2O3" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.MnO].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.MnO].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "MnO" + excessSuffix, typeof(double));

            if (data.Any(o => o.ElementsLeft[(int)ElementNamesEnum.Ba].NormValue.HasValue ? o.ElementsLeft[(int)ElementNamesEnum.Ba].NormValue.Value > 0 : false))
                table.Columns.Add(excessPrefix + "Ba" + excessSuffix, typeof(double));

            foreach (T item in data)
            {
                double nCoefficient=1;

                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                {
                    if (!prop.Name.Contains("Elements") && prop.Name != "ProfileAssigned")
                        row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                }

                if (norm) nCoefficient = 100/item.MineralElementsTotalPercent;

                for (int i = 0; i < minerals.Count; i++)
                {
                    row[minerals[i].OutputName != null ? minerals[i].OutputName : minerals[i].Name] = item.MineralsElementsCaculated[i].WtPercent * nCoefficient;
                }

                foreach (string cat in minerals.Where(m => !String.IsNullOrEmpty(m.Category)).GroupBy(m => m.Category).Select(g => g.First().Category).OrderBy(grp=>grp.ToLower()))
                {
                    row[cat] = item.GetMineralElementsCategoryTotalPercent(cat) * nCoefficient;
                }

                //row["Other"] = item.GetMineralElementsCategoryTotalPercent("") * nCoefficient;
                row["Mineral Total"] = nCoefficient==1? item.MineralElementsTotalPercent: 100;

                if (row.Table.Columns.Contains(excessPrefix + "Na2O" + excessSuffix))
                    row[excessPrefix + "Na2O" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.Na2O].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.Na2O].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "MgO" + excessSuffix))
                    row[excessPrefix + "MgO" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.MgO].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.MgO].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "Al2O3" + excessSuffix))
                    row[excessPrefix + "Al2O3" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.Al2O3].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.Al2O3].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "SiO2" + excessSuffix))
                    row[excessPrefix + "SiO2" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.SiO2].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.SiO2].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "P2O5" + excessSuffix))
                    row[excessPrefix + "P2O5" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.P2O5].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.P2O5].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "SO3" + excessSuffix))
                    row[excessPrefix + "SO3" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.SO3].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.SO3].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "Cl" + excessSuffix))
                    row[excessPrefix + "Cl" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.Cl].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.Cl].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "K2O" + excessSuffix))
                    row[excessPrefix + "K2O" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.K2O].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.K2O].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "CaO" + excessSuffix))
                    row[excessPrefix + "CaO" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.CaO].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.CaO].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "TiO2" + excessSuffix))
                    row[excessPrefix + "TiO2" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.TiO2].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.TiO2].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "Fe2O3" + excessSuffix))
                    row[excessPrefix + "Fe2O3" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.Fe2O3].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.Fe2O3].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "MnO" + excessSuffix))
                    row[excessPrefix + "MnO" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.MnO].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.MnO].NormValue.Value : 0;

                if (row.Table.Columns.Contains(excessPrefix + "Ba" + excessSuffix))
                    row[excessPrefix + "Ba" + excessSuffix] = item.ElementsLeft[(int)ElementNamesEnum.Ba].NormValue.HasValue ? item.ElementsLeft[(int)ElementNamesEnum.Ba].NormValue.Value : 0;

                table.Rows.Add(row);
            }
            return table;
        }

        public static List<T> ToRockWiseDataList<T>(this DataTable dt, int? lessThanValueFactor,bool showLessThanSym, CultureInfo ci) where T : RockWiseCommon, new()
        {
            var myEnumerable = dt.AsEnumerable();
            DateTime evdate;

            List<T> myClassList =
                (from item in myEnumerable
                 where (DateTime.TryParse(item.Field<string>(3),ci.DateTimeFormat,DateTimeStyles.None, out evdate) || typeof(IProfileData).IsAssignableFrom(typeof(T)))
                 select (T)Activator.CreateInstance(typeof(T), item, lessThanValueFactor,showLessThanSym,ci)
                 ).ToList();
            return myClassList;
        }

        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (T)attributes[0];
        }

        public static string generateCsvString(DataGridView dgv, bool allCols, string selectColName)
        {
            string strExport = "";
            try
            {
                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    DataGridViewColumn dc = dgv.Columns.Cast<DataGridViewColumn>().Where(c => c.DisplayIndex == i).First();
                    if ((allCols || ((string)dc.Tag).StartsWith("save")) && dc.Name != selectColName && (dc.HeaderCell.GetType() == typeof(DatagridViewCheckBoxHeaderCell) ? ((DatagridViewCheckBoxHeaderCell)dc.HeaderCell).Checked : true))
                        strExport += dc.Name + ",";
                }

                if (!String.IsNullOrEmpty(strExport))
                {
                    strExport = strExport.Substring(0, strExport.Length - 1) + Environment.NewLine.ToString();
                    foreach (DataGridViewRow dr in dgv.Rows)
                    {
                        if (!String.IsNullOrEmpty(selectColName))
                            if (!(bool)dr.Cells[selectColName].Value)
                                continue;
                        for (int i = 0; i < dgv.Columns.Count; i++)
                        {
                            DataGridViewColumn dc = dgv.Columns.Cast<DataGridViewColumn>().Where(c => c.DisplayIndex == i).First();
                            if ((allCols || ((string)dc.Tag).StartsWith("save")) && dc.Name != selectColName && (dc.HeaderCell.GetType() == typeof(DatagridViewCheckBoxHeaderCell) ? ((DatagridViewCheckBoxHeaderCell)dc.HeaderCell).Checked : true))
                            {
                                if (dr.Cells[dc.Index].Value != DBNull.Value || ((string)dc.Tag) == "savet")
                                {
                                    if (dr.Cells[dc.Index].ValueType == typeof(RockWiseValue))
                                    {
                                        RockWiseValue dv = (RockWiseValue)dr.Cells[dc.Index].Value;
                                        strExport += dv.ToString();
                                    }
                                    else if (dr.Cells[dc.Index].Value.ToString() != "-1")
                                        strExport += dr.Cells[dc.Index].Value.ToString();
                                    else
                                        strExport += "Tr";
                                }
                                else
                                    strExport += "0";
                                strExport += ",";
                            }
                        }
                        strExport = strExport.Substring(0, strExport.Length - 1) + Environment.NewLine.ToString();
                    }
                }
                return strExport;
            }
            catch
            {
                return "";
            }
            finally
            {
            }

        }

        public static void CopyColumnsToClipboard(DataGridView dgv, bool includeHeader, string selectColName)
        {
	        string s = "";
            if (includeHeader)
            {
                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    DataGridViewColumn dc = dgv.Columns.Cast<DataGridViewColumn>().Where(c => c.DisplayIndex == i).First();
                    if (((string)dc.Tag).StartsWith("save"))
                        s += dc.HeaderText + "\t";
                }
                s = s.Substring(0, s.Length - 1);
                s += Environment.NewLine;
            }

	        foreach (DataGridViewRow row in dgv.Rows) 
            {
                if (!String.IsNullOrEmpty(selectColName))
                    if (!(bool)row.Cells[selectColName].Value)
                        continue;

                for (int i = 0; i < dgv.Columns.Count; i++)
                {
                    DataGridViewColumn dc = dgv.Columns.Cast<DataGridViewColumn>().Where(c => c.DisplayIndex == i).First();
                    if (((string)dc.Tag).StartsWith("save"))
                    {
                        if (row.Cells[dc.Index].Value != DBNull.Value||(string)dc.Tag=="savet")
                        {
                            if (row.Cells[dc.Index].Value.ToString() != "-1")
                                s += row.Cells[dc.Index].Value.ToString();
                            else
                                s += "Tr";
                        }
                        else
                            s += "0";
                        s += "\t";
                    }
                }
		        s = s.Substring(0, s.Length - 1);
		        s += Environment.NewLine;
	        }

            DataObject o = new DataObject();
            if (!String.IsNullOrEmpty(s))
            {
                o.SetText(s);
                Clipboard.SetDataObject(o, true);
            }
        }

    }
}
