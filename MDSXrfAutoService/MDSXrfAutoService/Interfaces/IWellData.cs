﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XrfWindowsService
{
    interface IWellData
    {
        bool Select { get; set; }
        string Well { get; set; }
        string Method { get; set; }
        DateTime EvalTime { get; set; }
        RockWiseProfileData ProfileAssigned { get; set; }
        CalibrationSet ElementsCalibrationSet { get; set; }
    }
}
