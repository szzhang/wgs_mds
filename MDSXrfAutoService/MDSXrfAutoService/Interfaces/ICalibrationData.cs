﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XrfWindowsService
{
    interface ICalibrationData : IWellData
    {
        string ProfileNameFromOrigFile { get; set; }
    }
}
