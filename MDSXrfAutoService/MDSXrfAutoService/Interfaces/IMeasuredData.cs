﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XrfWindowsService
{
    interface IMeasuredData : IWellData
    {
        double Depth { get; set; }
    }
}
