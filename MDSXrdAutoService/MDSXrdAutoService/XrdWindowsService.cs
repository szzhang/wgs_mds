﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;

namespace WgsMDS
{
    public partial class XrdWindowsService : ServiceBase
    {
        PollingWorker pr;
        private static int numLogOutputLines = 0;
        private static XrdMineralsList xrdPhases = new XrdMineralsList();
        private static int runningProc = 0;
        private static int endedProc = 0;

        public XrdWindowsService()
        {
            InitializeComponent();

            System.IO.Directory.SetCurrentDirectory(System.AppDomain.CurrentDomain.BaseDirectory); 
            pr = new PollingWorker(5000, XrdWindowsService.CheckAndProcess, null);
        }

        static XrdWindowsService()
        {
            StreamReader str = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MinStrs.xml"));
            XmlSerializer xSerializer = new XmlSerializer(typeof(XrdMineralsList));
            xrdPhases = (XrdMineralsList)xSerializer.Deserialize(str);
            str.Close();
        }

        protected override void OnStart(string[] args)
        {
            pr.StartWork(true);
        }

        protected override void OnStop()
        {
            pr.PauseWork();
        }

        protected override void OnPause()
        {
            base.OnPause();
            pr.PauseWork();
        }

        protected override void OnContinue()
        {
            base.OnContinue();
            pr.StartWork();
        }

        protected override void OnShutdown()
        {
            base.OnShutdown();
            pr.Quit();
        }

        private static void LogToFile(string eventText)
        {
            string logFileNameBase = Properties.Settings.Default.logFileName;
            // Collect the sort command output. 
            if (!String.IsNullOrEmpty(eventText))
            {
                numLogOutputLines++;

                string logfile = logFileNameBase + DateTime.Now.ToString("_MMddyyyy") + ".log";
                if (!File.Exists(logfile))
                {
                    File.Create(logfile).Close();
                    numLogOutputLines = 0;
                }

                File.AppendAllText(logfile, Environment.NewLine +
                    "[" + numLogOutputLines.ToString() + "] - " + DateTime.Now.ToString() + " - " + eventText);
            }
        }

        private static void CheckAndProcess(object state)
        {
            Dictionary<string, double> csvOut = new Dictionary<string, double>();
            double dnumber;

            try
            {
                string rawLoc = Properties.Settings.Default.rawFilesDir;
                string topasLoc = Properties.Settings.Default.topasDir;
                //string fileINP = Properties.Settings.Default.fileINP;
                string fileTopasOutput = Properties.Settings.Default.fileTopasOutput;
                string fileTopasOutputRWP = Properties.Settings.Default.fileTopasOutputRWP;
                string fileCSV = Properties.Settings.Default.fileCSV;

                if (Directory.Exists(rawLoc))
                {
                    string pattern = @"(?i)(\d+)\.raw$";
                    List<string> files = Directory.GetFiles(rawLoc).Where(path => Regex.Match(Path.GetFileName(path), pattern).Success).ToList();

                    if (files.Count > 0)
                    {
                        fileCSV = fileCSV + DateTime.Now.ToString("_MMddyyyy") + ".csv";
                        if (!File.Exists(fileCSV))
                        {
                            File.Create(fileCSV).Close();
                        }
                    }

                    for (int i = 0; i < files.Count; i++)
                    {
                        LogToFile("Processing file - " + files[i]);

                        string rep = @"(?i)((_\d+)\.inp$|.*\.out$)";
                        DirectoryInfo repdi = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
                        FileInfo[] rpfiles = repdi.GetFiles().Where(path => Regex.Match(Path.GetFileName(path.FullName), rep).Success).ToArray();
                        foreach (FileInfo pfile in rpfiles)
                            try
                            {
                                File.Delete(pfile.FullName);
                            }
                            catch { }
                        

                        csvOut.Clear();
                        Match m = Regex.Match(Path.GetFileName(files[i]), pattern);
                        csvOut.Add("D_depth_m", double.TryParse(m.Value.Substring(0, m.Value.Length - 4), out dnumber) ? dnumber : 0);

                        LogToFile("Calculating minerals profile for " + files[i]);
                        string inpf = excecuteTOPASCompare(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BasePhaserExportScript.inp"), new List<int>(), i, files[i]);

                        if (string.IsNullOrEmpty(inpf))
                        {
                            LogToFile("Cannot find minerals profile for " + files[i]);
                            continue;
                        }

                        LogToFile("Finished calculating minerals profile " + inpf);
                        List<string> newLines = File.ReadAllLines(inpf).ToList();
                        int indx = newLines.FindIndex(str => str.Contains("_rwp.out"));
                        if (indx == -1)
                        {
                            newLines.Add("out_rwp " + fileTopasOutputRWP);
                            newLines.Add("out " + fileTopasOutput);
                        }
                        else
                        {
                            newLines[indx] = "out_rwp " + fileTopasOutputRWP;
                            newLines[indx + 1] = "out " + fileTopasOutput;
                        }

                        File.WriteAllLines(inpf, newLines);


                        //for (int j = 0; j < xrdPhases.Minerals.Count; j++)
                        //{
                        //    string[] lines = File.ReadAllLines();

                        //    int indx = lines.ToList().FindIndex(str => str.Contains("'hereisthestr"));
                        //    string[] vstrs = xrdPhases[j].Value.Split('\n');

                        //    List<string> newLines = lines.ToList();
                        //    newLines.InsertRange(indx, vstrs);

                        //    indx = newLines.FindIndex(str => str.Contains("***"));
                        //    newLines[indx] = newLines[indx].Replace("***", xrdPhases[j].Variable);

                        //    newLines.Add("out_rwp "+fileTopasOutputRWP+"\r\n");
                        //    newLines.Add("out "+fileTopasOutput+"\r\n");                            
                        //    newLines.Add("Out("+ xrdPhases[j].Variable+", \""+xrdPhases[j].Name+":%f\\n\")"+"\r\n");

                        //    string fileINP = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"BasePhaserExportScript_" + i.ToString() + "_" + j.ToString() + ".inp");
                        //    File.WriteAllLines(fileINP, newLines);

                        using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                        {
                            process.StartInfo = new System.Diagnostics.ProcessStartInfo();
                            process.StartInfo.WorkingDirectory = topasLoc;
                            process.StartInfo.FileName = Path.Combine(topasLoc, "tc.exe");
                            process.StartInfo.Arguments = @" " + inpf + " \"macro FileName {" + files[i] + "}\"";
                            process.StartInfo.CreateNoWindow = true;

                            process.StartInfo.RedirectStandardOutput = true;
                            process.StartInfo.RedirectStandardError = true;
                            process.StartInfo.RedirectStandardInput = true;

                            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            process.StartInfo.UseShellExecute = false;

                            process.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(onOutputDataReceived);
                            process.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler(onOutputDataReceived);
                            process.Start();
                            process.BeginOutputReadLine();
                            process.BeginErrorReadLine();
                            process.WaitForExit();
                        }

                        //    using (StreamReader sr = new StreamReader(fileTopasOutputRWP))
                        //    {
                        //        String line = "";
                        //        while (sr.EndOfStream == false)
                        //        {
                        //            line = sr.ReadLine();
                        //        }
                        //        double rwpV = double.TryParse(line, out dnumber) ? dnumber : 1000000;
                        //        xrdPhases[j].RWPValue=rwpV;
                        //    }

                        //    //if (!process.HasExited)
                        //    //{
                        //    //    process.WaitForExit(120000); // give 2 minutes for process to finish
                        //    //    if (!process.HasExited)
                        //    //    {
                        //    //        process.Kill(); // took too long, kill it off
                        //    //    }
                        //    //}
                        //}
                        //int idx = xrdPhases.Minerals.FindIndex(mi => mi.RWPValue == xrdPhases.Minerals.Min(o => o.RWPValue));
                        //string ppattern = @"(?i)((_(?!" + idx.ToString() + @"\b)\d+)\.inp$|.*\.out$)";

                        //DirectoryInfo pdi = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
                        //FileInfo[] pfiles = pdi.GetFiles().Where(path => Regex.Match(Path.GetFileName(path.FullName), ppattern).Success).ToArray();

                        //foreach (FileInfo pfile in pfiles)
                        //    try
                        //    {
                        //        File.Delete(pfile.FullName);
                        //    }
                        //    catch { }

                        DirectoryInfo di = new DirectoryInfo(rawLoc);
                        DirectoryInfo dis = di.CreateSubdirectory("ProcessedRaw");

                        moveFile(files[i], di.GetDirectories("ProcessedRaw").First().FullName);

                        using (StreamReader sr = new StreamReader(fileTopasOutput))
                        {
                            String line;
                            while ((line = sr.ReadLine()) != null)
                            {
                                string[] ps = line.Split(':');
                                csvOut.Add(ps[0], Math.Round(Convert.ToDouble(ps[1])));
                            }
                        }

                        using (StreamReader sr = new StreamReader(fileTopasOutputRWP))
                        {
                            String line ="";
                            while (sr.EndOfStream == false)
                            {
                                line = sr.ReadLine();
                            }
                            double rwpV = double.TryParse(line, out dnumber) ? dnumber : 1000000;
                            csvOut.Add("XRD_RWP", rwpV);
                            if (rwpV>15)
                                csvOut.Add("Valid", 0);
                            else
                                csvOut.Add("Valid", 1);
                        }

                        if (!csvOut.Any()) continue;
                        StringBuilder writer = new StringBuilder();
                        // Generating Header.
                        List<string> headers = csvOut.Keys.Select(x => x).ToList();
                        if (i == 0)
                        {
                            if (new FileInfo(fileCSV).Length == 0)
                            {
                                // file is empty
                                writer.Append(string.Join(", ", headers.Select(h => h)));
                                writer.AppendLine(", filename");
                            }
                        }                        // Generating content.
                        writer.Append(string.Join(", ", headers.Select(h => csvOut[h])));
                        writer.AppendLine(", " + Path.GetFileName(files[i]));

                        File.AppendAllText(fileCSV, writer.ToString());
                    }
                }
            }
            catch (Exception e)
            {
                LogToFile(e.Message);
            }
        }

        private static string excecuteTOPASCompare(string inpFile, List<int> excludeMinIndexes, int fileIdx, string fileN)
        {
            double dnumber;
            string folderTopasOutput = Properties.Settings.Default.fileTopasOutput;
            string folderTopasOutputRWP = Properties.Settings.Default.fileTopasOutputRWP;
            string topasLoc = Properties.Settings.Default.topasDir;
            //int numProcess = 0;

            for (int j = 0; j < xrdPhases.Minerals.Count; j++)
            {
                if (excludeMinIndexes.Contains(j)) continue;
                string[] lines = File.ReadAllLines(inpFile);

                int indx = lines.ToList().FindIndex(str => str.Contains("'hereisthestr"));
                string[] vstrs = xrdPhases[j].Value.Split('\n');

                List<string> newLines = lines.ToList();
                newLines.InsertRange(indx, vstrs);

                indx = newLines.FindIndex(str => str.Contains("***"));
                newLines[indx] = newLines[indx].Replace("***", xrdPhases[j].Variable);

                string fileINP = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "BasePhaserExportScript_" + fileIdx.ToString() + "_" + excludeMinIndexes.Count.ToString() + "_" + j.ToString() + ".inp");
                indx = newLines.FindIndex(str => str.Contains("_rwp.out"));
                if (indx == -1)
                {
                    newLines.Add("out_rwp " + fileINP.ToLower().Replace(".inp", "_rwp.out"));
                    newLines.Add("out " + fileINP.ToLower().Replace(".inp", "_data.out"));
                }
                else
                {
                    newLines[indx] = "out_rwp " + fileINP.ToLower().Replace(".inp", "_rwp.out");
                    newLines[indx+1] = "out " + fileINP.ToLower().Replace(".inp", "_data.out");
                }
                
                newLines.Add("Out(" + xrdPhases[j].Variable + ", \"" + xrdPhases[j].Name + ":%f\\n\")" + "\r\n");

                File.WriteAllLines(fileINP, newLines);
                //numProcess++;
            }

            if (excludeMinIndexes.Count > 0)
            {
                try
                {
                    File.Delete(inpFile);
                }
                catch { }
            }

            string ppattern = @"(?i)(_\d+)\.inp$";
            DirectoryInfo pdi = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            FileInfo[] pfiles = pdi.GetFiles().Where(path => Regex.Match(Path.GetFileName(path.FullName), ppattern).Success).ToArray();
            runningProc = pfiles.Count();

            foreach (FileInfo f in pfiles)
            {
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo = new System.Diagnostics.ProcessStartInfo();
                process.StartInfo.WorkingDirectory = topasLoc;
                process.StartInfo.FileName = Path.Combine(topasLoc, "tc.exe");
                process.StartInfo.Arguments = @" " + f.FullName + " \"macro FileName {" + fileN + "}\"";
                process.EnableRaisingEvents = true;
                process.Exited += new EventHandler(myProcess_Exited);
                process.Start();
            }

            while (endedProc < runningProc)
            {
            }

            runningProc = 0;
            endedProc = 0;

            ppattern = @"(?i)_rwp\.out$";
            pdi = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            pfiles = pdi.GetFiles().Where(path => Regex.Match(Path.GetFileName(path.FullName), ppattern).Success).ToArray();

            foreach (FileInfo f in pfiles)
            {
                using (StreamReader sr = new StreamReader(f.FullName))
                {
                    String line = "";
                    while (sr.EndOfStream == false)
                    {
                        line = sr.ReadLine();
                    }
                    double rwpV = double.TryParse(line, out dnumber) ? dnumber : 1000000;
                    int idxx = Convert.ToInt32(f.Name.Split('_').ElementAt(3));
                    xrdPhases[idxx].RWPValue = rwpV;
                }
            }

            int idx = xrdPhases.Minerals.FindIndex(mi => mi.RWPValue == xrdPhases.Minerals.Min(o => o.RWPValue));
            double minR = xrdPhases.Minerals.Where(min => min.RWPValue.HasValue).Min(o => o.RWPValue.Value);
            xrdPhases.Minerals.ForEach(mo => mo.RWPValue = (double?)null);

            ppattern = @"(?i)((_(?!" + idx.ToString() + @"\b)\d+)\.inp$|.*\.out$)";
            pdi = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            pfiles = pdi.GetFiles().Where(path => Regex.Match(Path.GetFileName(path.FullName), ppattern).Success).ToArray();
            foreach (FileInfo pfile in pfiles)
                try
                {
                    File.Delete(pfile.FullName);
                }
                catch { }

            ppattern = @"(?i)_" + idx.ToString() + @"\b\.inp$";
            pdi = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            pfiles = pdi.GetFiles().Where(path => Regex.Match(Path.GetFileName(path.FullName), ppattern).Success).ToArray();

            if (minR > 15)
            {
                excludeMinIndexes.Add(idx);
                if (excludeMinIndexes.Count() == xrdPhases.Minerals.Count())
                    return "";
                else
                    return excecuteTOPASCompare(pfiles.First().FullName, excludeMinIndexes, fileIdx, fileN);
            }
            else
                return pfiles.First().FullName;
        }

        private static void myProcess_Exited(object sender, System.EventArgs e)
        {
            endedProc++;
        }

        private static void onOutputDataReceived(object sendingProcess, DataReceivedEventArgs outLine)
        {
            LogToFile(outLine.Data);
        }

        private static void moveFile(string f, string path)
        {
            if (File.Exists(Path.Combine(path,Path.GetFileName(f)))) 
            {
                File.Delete(Path.Combine(path, Path.GetFileName(f)));
            }
            System.IO.File.Move(f, Path.Combine(path,Path.GetFileName(f)));
        }
    }
}
